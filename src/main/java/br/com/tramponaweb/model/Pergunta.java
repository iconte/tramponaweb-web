package br.com.tramponaweb.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "pergunta")
public class Pergunta implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "descricao", nullable = false)
    private String descricao;
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "tipo", nullable = false)
    private TipoPergunta tipoPergunta;

    public Pergunta(Long id, String descricao, TipoPergunta tipoPergunta) {
	this.id = id;
	this.descricao = descricao;
	this.tipoPergunta = tipoPergunta;
    }

    public Pergunta() {
    }

    public String getDescricao() {
	return descricao;
    }

    public void setDescricao(String descricao) {
	this.descricao = descricao;
    }

    public TipoPergunta getTipoPergunta() {
	return tipoPergunta;
    }

    public void setTipoPergunta(TipoPergunta tipoPergunta) {
	this.tipoPergunta = tipoPergunta;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result
		+ ((descricao == null) ? 0 : descricao.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
	if (this == obj) {
	    return true;
	}
	if (obj == null) {
	    return false;
	}
	if (!(obj instanceof Pergunta)) {
	    return false;
	}
	Pergunta other = (Pergunta) obj;
	if (descricao == null) {
	    if (other.descricao != null) {
		return false;
	    }
	} else if (!descricao.equalsIgnoreCase(other.descricao)) {
	    return false;
	}
	return true;
    }

}
