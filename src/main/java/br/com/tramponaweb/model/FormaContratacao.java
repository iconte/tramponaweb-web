package br.com.tramponaweb.model;

public enum FormaContratacao {
    CLT("Efetivo"), PJ("Prestador Serviço"), Flex("Flex");
    private String label;

    private FormaContratacao(String label) {
	this.label = label;
    }

    public String getLabel() {
	return label;
    }
}
