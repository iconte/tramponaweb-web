package br.com.tramponaweb.model;

import java.io.Serializable;

public enum TipoPergunta implements Serializable{
    UNICA("Única"),MULTIPLA("Multipla");
    private String label;

    private TipoPergunta(String label) {
	this.label = label;
    }

    public String getLabel() {
	return label;
    }
}
