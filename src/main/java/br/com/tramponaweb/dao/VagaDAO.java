package br.com.tramponaweb.dao;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import br.com.tramponaweb.model.Vaga;

@Stateless
public class VagaDAO implements Serializable {
    private static final long serialVersionUID = 1L;

//    @PersistenceContext(name = "tramponaweb", type = PersistenceContextType.TRANSACTION)
    private EntityManager entityManager;

    public VagaDAO() {
    }

    public VagaDAO(EntityManager entityManager) {
	this.entityManager = entityManager;
    }

    @SuppressWarnings("unchecked")
    public List<Vaga> listarTodos() {
	return entityManager.createQuery("select c FROM Vaga c")
		.getResultList();
    }

    public EntityManager getEntityManager() {
	return entityManager;
    }

    public void setEntityManager(EntityManager entityManager) {
	this.entityManager = entityManager;
    }

    public void salvar(Vaga vaga) {
	try {
	    entityManager.merge(vaga);
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    public void excluir(Long id) {
	try {
	    Vaga vagaRemove = obterPorId(id);
	    entityManager.remove(vagaRemove);
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    private Vaga obterPorId(Long id) {
	return entityManager.find(Vaga.class, id);
    }
}
