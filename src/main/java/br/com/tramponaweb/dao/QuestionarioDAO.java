package br.com.tramponaweb.dao;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import br.com.tramponaweb.model.Questionario;

@Stateless
public class QuestionarioDAO implements Serializable {
    private static final long serialVersionUID = 1L;

//    @PersistenceContext(name = "tramponaweb", type = PersistenceContextType.TRANSACTION)
    private EntityManager entityManager;

    public QuestionarioDAO() {
    }

    public QuestionarioDAO(EntityManager entityManager) {
	this.entityManager = entityManager;
    }

    @SuppressWarnings("unchecked")
    public List<Questionario> listarTodos() {
	return entityManager.createQuery("select c FROM Questionario c")
		.getResultList();
    }

    public EntityManager getEntityManager() {
	return entityManager;
    }

    public void setEntityManager(EntityManager entityManager) {
	this.entityManager = entityManager;
    }

    public void salvar(Questionario questionario) {
	try {
	    entityManager.merge(questionario);
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    public void excluir(Long id) {
	try {
	    Questionario questionariooRemove = obterPorId(id);
	    entityManager.remove(questionariooRemove);
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    public Questionario obterPorId(Long id) {
	return entityManager.find(Questionario.class, id);
    }
}

