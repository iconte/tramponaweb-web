package br.com.tramponaweb.lazy;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import br.com.tramponaweb.dao.QuestionarioDAO;
import br.com.tramponaweb.model.Questionario;

public class QuestionarioLazy extends LazyDataModel<Questionario> implements
		Serializable {
	@Inject
	private QuestionarioDAO questionarioDAO;
	private static final long serialVersionUID = -2282012648135375826L;

	@Override
	public List<Questionario> load(int first, int pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) {
		List<Questionario> listarTodos = questionarioDAO.listarTodos();
		int size = listarTodos.size();
		setRowCount(size);
		// paginate
		if (size > pageSize) {
			try {
				return listarTodos.subList(first, first + pageSize);
			} catch (IndexOutOfBoundsException e) {
				return listarTodos.subList(first, first
						+ (size % pageSize));
			}
		}
		return listarTodos;
	}

}
