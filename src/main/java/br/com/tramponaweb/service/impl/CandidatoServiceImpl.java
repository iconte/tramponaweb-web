package br.com.tramponaweb.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import br.com.tramponaweb.model.Candidato;
import br.com.tramponaweb.repository.CandidatoRepository;
import br.com.tramponaweb.service.CandidatoService;

public class CandidatoServiceImpl implements CandidatoService, Serializable {

    private static final long serialVersionUID = 1L;
    @Inject
    private CandidatoRepository candidatoRepository;

    @Override
    public void salvar(Candidato candidato) {
	candidatoRepository.save(candidato);
    }

    @Override
    public Candidato obterPorId(Long id) {
	return candidatoRepository.findBy(id);
    }

    @Override
    public List<Candidato> listar() {
	return candidatoRepository.findAll();
    }

    @Override
    public void excluir(Long id) {
	Candidato obterPorId = obterPorId(id);
	candidatoRepository.remove(obterPorId);
    }
}
