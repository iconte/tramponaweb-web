package br.com.tramponaweb.repository;

import java.io.Serializable;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;

import br.com.tramponaweb.model.Candidato;

@Repository
public interface CandidatoRepository extends EntityRepository<Candidato, Serializable> {
    
}
