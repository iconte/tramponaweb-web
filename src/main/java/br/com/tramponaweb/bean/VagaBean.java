package br.com.tramponaweb.bean;

import java.io.Serializable;
import java.util.Date;

import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.tramponaweb.dao.VagaDAO;
import br.com.tramponaweb.lazy.VagaLazy;
import br.com.tramponaweb.model.Vaga;

@Named
@ViewScoped
public class VagaBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private Vaga vaga;
    @Inject
    private VagaLazy vagaLazy;
    @Inject
    private VagaDAO vagaDAO;

    public VagaBean() {
	this.vaga = new Vaga();
    }

    public void limpar() {
	this.vaga = new Vaga();
    }

    public void salvar() {
	try {
	    vagaDAO.salvar(vaga);
	    limpar();
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    public void excluir(Long id) {
	try {
	    vagaDAO.excluir(id);
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    public Vaga getVaga() {
	return vaga;
    }

    public void setVaga(Vaga vaga) {
	this.vaga = vaga;
    }

    public VagaLazy getVagaLazy() {
	return vagaLazy;
    }

    public void setVagaLazy(VagaLazy vagaLazy) {
	this.vagaLazy = vagaLazy;
    }
    public Date getMinDataPublicacao(){
	return new Date();
    }
}
