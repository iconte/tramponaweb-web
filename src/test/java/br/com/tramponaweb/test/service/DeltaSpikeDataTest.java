package br.com.tramponaweb.test.service;

import br.com.tramponaweb.model.Candidato;
import br.com.tramponaweb.service.CandidatoService;
import com.github.dbunit.rules.cdi.api.UsingDataSet;
import org.apache.deltaspike.core.api.projectstage.ProjectStage;
import org.apache.deltaspike.testcontrol.api.TestControl;
import org.apache.deltaspike.testcontrol.api.junit.CdiTestRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.util.List;

import static com.github.dbunit.rules.cdi.util.EntityManagerProvider.tx;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(value = CdiTestRunner.class)
@TestControl(projectStage = ProjectStage.UnitTest.class)
public class DeltaSpikeDataTest {

	@Inject
	private CandidatoService candidatoService;

	@Test
	@UsingDataSet("candidato.yml")
	public void mustListCandidato() {
		Candidato candidato1 = new Candidato();
		candidato1.setId(1L);
		List<Candidato> cadidatos = candidatoService.listar();
		assertThat(cadidatos).isNotNull().hasSize(2).contains(candidato1);
	}

	@Test
	@UsingDataSet("candidato.yml")
	public void mustUpdateCandidato() {
		Candidato candidato = candidatoService.obterPorId(1L);
		assertThat(candidato).isNotNull();
		assertThat(candidato.getNome()).isEqualTo("candidato1");
		candidato.setNome("candidato updated");
		candidatoService.salvar(candidato);
		candidato = candidatoService.obterPorId(1L);
		assertThat(candidato.getNome()).isEqualTo("candidato updated");
	}

	@Test
	@UsingDataSet("candidato.yml")
	public void mustDeleteCandidato() {
		candidatoService.excluir(1L);
		Candidato candidato  = candidatoService.obterPorId(1L);
		assertThat(candidato).isNull();
	}

	@Test
	@UsingDataSet("candidato.yml")
	public void mustFindCandidato() {
		Candidato candidato  = candidatoService.obterPorId(2L);
		Candidato candidato2 = new Candidato();
		candidato2.setId(2L);
		assertThat(candidato).isNotNull().isEqualTo(candidato2);
		assertThat(candidato.getEmail()).isNotNull().isEqualTo("b@b.com");

	}




}
