package br.com.tramponaweb.test.service;

import javax.inject.Inject;

import org.apache.deltaspike.testcontrol.api.junit.CdiTestRunner;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import br.com.tramponaweb.service.TesteService;

@RunWith(value = CdiTestRunner.class)
public class BasicTest {

	@Inject
	private TesteService testeService;

	@Test
	public void mustReturnStringTest() {
		String test = testeService.getTest();
		Assert.assertEquals("test", test);	
	}

}
