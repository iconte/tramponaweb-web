package br.com.tramponaweb.test.dao;


import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.tramponaweb.dao.CandidatoDAO;
import br.com.tramponaweb.model.Candidato;


public class CandidatoDAOTest {
	EntityManager entityManager;
	EntityManagerFactory factory;
	CandidatoDAO candidatoDAO;

	@Before
	public void antes() {
		try {
			factory= Persistence.createEntityManagerFactory("teste");
			entityManager = factory.createEntityManager();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
//	@Test
	public void criarCandidatos(){
		candidatoDAO =  new CandidatoDAO(entityManager);
		for (int i = 1; i < 2; i++) {
			Candidato candidato = new Candidato();
			candidato.setNome("Candidato"+i);
			candidato.setEnd("end"+i);
			candidato.setTel("999988"+i);
			candidato.setDataNasc(new Date());
			
			candidatoDAO.salvar(candidato);
			System.out.println(candidato);
		}
		
	}
//	@Test
	public void listarCandidatos(){
		candidatoDAO =  new CandidatoDAO(entityManager);
		List<Candidato>candidatos= candidatoDAO.listarTodos();
		for (Candidato candidato : candidatos) {
			System.out.println(candidato);
		}
	}


	@After
	public void depois() {
		entityManager.close();
		factory.close();
	}

}
